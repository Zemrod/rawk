use arguments;
use std::env;

pub mod config;

#[cfg(test)]
mod tests;

/// the help message
pub const HELP: &str =
    "Syntax to call the program:\nrawk [-s <seperator_char>] [-l <n>] [-f <n>] <filename>\n
    the program is able to read from stdin\n
    -s the char following this flag will be seen as a seperator, picked in combination with -f\n
    -l the line number\n
    -f the sperator seperates the line this number chooses the fragment";

/// creates additional options that are not considered as such by default i.e `-l`
pub fn make_option(arg: &mut arguments::Arguments, para: &str) {
    let mut opt: Vec<String> = Vec::new();
    let mut pos: usize = 0;
    let mut done: bool = false;

    for s in 0..arg.orphans.len() {
        // if the flag is done and it's following value considered continue
        if done && s == pos {
            continue;
        } else if arg.orphans[s] != String::from(para) {
            // push a copy into the vector opt
            opt.push(arg.orphans[s].clone());
        } else {
            // ignore the '-' and put the letter and it's value to arg.options
            let mut st: String = String::from(para);
            st.retain(|c| c != '-');

            // make the flag and its value an actual option
            arg.options.set(st.as_str(), arg.orphans[s + 1].clone());
            pos = s + 1;
            done = true;
        }
    }

    // change arg.orphans to the newly filtered version
    arg.orphans = opt;
}

/// parses the commandline arguments
pub fn arg_parse() -> Result<arguments::Arguments, arguments::Error> {
    // get the commandline arguments
    let args: env::Args = env::args();

    // parse the commandline arguments in a special struct
    let arg: arguments::Arguments = arguments::parse(args)?;

    Ok(arg)
}

/// run the program
pub fn run(config: &mut config::Config) -> Result<Vec<String>, &'static str> {
    use std::process;

    // determine the seperator
    let sep: char = match config.sep {
        Some(c) => c,
        None => ' ',
    };

    // if a line number has been passed set it, else it is 0
    let line: usize = match config.line {
        Some(l) => l,
        None => 0,
    };

    let mut res: Vec<String> = Vec::new();
    let tmp: Vec<String> = config.content.lines().map(|s| String::from(s)).collect();

    // if the line does not exist print error and terminate
    if tmp.len() < line {
        if line == 0 {
            return Err("Linenumber less than 1 is invalid");
        } else {
            eprintln!(
                "Line: {} does not exist the file only has: {} lines",
                line,
                tmp.len()
            );
            process::exit(1);
        }
    // push line if exists
    } else if line > 0 {
        res.push(tmp[line - 1].clone());
    } else {
        res = tmp;
    }

    let mut end: Vec<String> = Vec::new();
    let mut tmp: String = String::new();

    // seperate the string at the seperator
    for line in res.iter() {
        for c in line.chars() {
            if c == sep {
                end.push(tmp.clone());
                tmp = String::new();
            } else {
                tmp.push(c);
            }
        }
        end.push(tmp.clone());
    }

    // consider multiple seperators directler after a previoos one
    end.retain(|s| *s != String::from(""));

    Ok(end)
}

/// final process of the program
pub fn processing(result: Vec<String>, f: usize) -> Result<String, &'static str> {
    let mut output: String = String::new();
    let fr: usize = if f == 0 { 0 } else { f - 1 };

    // if fraction does not exist return error and terminate
    if fr >= result.len() {
        return Err("fraction number exceeds fractions");
    } else if f == 0 {
        for frac in result.iter() {
            output.push_str(frac);
        }
    } else {
        output = result[fr].clone();
    }

    Ok(output)
}
