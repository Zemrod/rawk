use super::config::Config;

#[test]
fn reading() {
    let config: Config = Config {
        content: String::new(),
        frac: None,
        sep: None,
        line: None,
        file: Some(String::from("test/file.txt")),
    };
    let file = "test/file.txt";
    let check = config.read_file(file);
    assert!(check.is_ok());
}

#[test]
fn content_test() {
    let mut config: Config = Config {
        content: String::new(),
        frac: None,
        sep: None,
        line: None,
        file: Some(String::from("test/file.txt")),
    };
    config.content = config.read_file("test/file.txt").unwrap();
    let check = String::from(
        "this is a file\nthe file has multiple lines\n2+2=fish\nwhich is just plain false\n",
    );

    assert_eq!(config.content, check);
}

#[test]
fn fetch_line() {
    let mut config: Config = Config {
        content: String::new(),
        frac: None,
        sep: None,
        line: None,
        file: Some(String::from("test/file.txt")),
    };
    config.content = config.read_file("test/file.txt").unwrap();
    let line: usize = 3;
    let mut res: Vec<String> = Vec::new();
    let tmp: Vec<String> = config.content.lines().map(|s| String::from(s)).collect();

    if tmp.len() < line {
        panic!("");
    } else if line > 0 {
        res.push(tmp[line - 1].clone());
    } else {
        res = tmp;
    }

    let expect = String::from("2+2=fish");

    assert_eq!(res[0], expect);
}

#[test]
fn name() {
    let line = String::from("2+2=fish");
    let sep = '=';
    let mut end: Vec<String> = Vec::new();
    let mut tmp: String = String::new();

    for c in line.chars() {
        if c == sep {
            end.push(tmp.clone());
            tmp = String::new();
        } else {
            tmp.push(c);
        }
    }
    end.push(tmp.clone());

    end.retain(|s| *s != String::from(""));

    let result = super::processing(end, 2);

    assert_eq!(result, Ok(String::from("fish")));
}
