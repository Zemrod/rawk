use rawk::{self, config};
use std::process;

fn main() {
    let mut parsed: arguments::Arguments = rawk::arg_parse().unwrap_or_else(|err| {
        eprintln!("Problem parsing the arguments: {}", err);
        process::exit(1);
    });

    // if --help has been called print the help message and end the program
    if let Some(_h) = parsed.get::<String>("help") {
        eprintln!("{}", rawk::HELP);
        process::exit(0);
    }

    rawk::make_option(&mut parsed, "-s");
    rawk::make_option(&mut parsed, "-f");
    rawk::make_option(&mut parsed, "-l");

    let mut config: config::Config = config::Config::new(&parsed).unwrap_or_else(|err| {
        eprintln!("{}", err);
        process::exit(1);
    });

    if let Some(file) = &config.file {
        config.content = config.read_file(file).unwrap_or_else(|err| {
            eprintln!("An error occured while reading the file: {}", err);
            process::exit(1);
        });
    } else {
        config.input().unwrap_or_else(|err| {
            eprintln!("An error occured while reading from stdin: {}", err);
            process::exit(1);
        });
    }

    // determine the fraction number
    let fraction: usize = match config.frac {
        Some(f) => f,
        None => 0,
    };

    let vec: Vec<String> = rawk::run(&mut config).unwrap_or_else(|err| {
        eprintln!("{}", err);
        process::exit(1);
    });

    let result: String = rawk::processing(vec, fraction).unwrap_or_else(|err| {
        eprintln!("{}", err);
        process::exit(1);
    });

    print!("{}", result);
}
