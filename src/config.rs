use arguments;
use std::error::Error;
use std::fs;
use std::io::{self, Read};

use crate::HELP;

/// the configuration of the program
pub struct Config {
    /// the line the program looks at
    pub line: Option<usize>,
    /// the seperator defaults to a space
    pub sep: Option<char>,
    /// the fraction of the string
    pub frac: Option<usize>,
    /// the content that has been read
    pub content: String,
    /// the name of the file to read
    pub file: Option<String>,
}

impl Config {
    /// the entire configuration process
    pub fn new(arg: &arguments::Arguments) -> Result<Config, &'static str> {
        // give line, sep and frac their values, if they don't exist they are None
        let line: Option<usize> = arg.get::<usize>("l");
        let sep: Option<char> = arg.get::<char>("s");
        let frac: Option<usize> = arg.get::<usize>("f");

        let content: String = String::new();
        // by default the file name is None
        let mut file: Option<String> = None;

        // catch a syntax error
        if arg.orphans.len() > 1 {
            return Err(HELP);
        } else if arg.orphans.len() == 1 {
            // extract the file name from the commandline args
            file = Some(arg.orphans[arg.orphans.len() - 1].clone());
        }

        // return the Config data wrapped in an Ok
        Ok(Config {
            line,
            sep,
            frac,
            content,
            file,
        })
    }

    /// opens and reads a file
    pub fn read_file(&self, filename: &str) -> Result<String, Box<dyn Error>> {
        // read from the file
        let content: String = fs::read_to_string(filename)?;

        Ok(content)
    }

    /// read from stdin
    pub fn input(&mut self) -> io::Result<()> {
        // read the input
        let mut stdin: io::Stdin = io::stdin();
        stdin.read_to_string(&mut self.content)?;

        Ok(())
    }
}
