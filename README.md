# Rawk
a tool inspired by gawk focusing on the ability to sepperate lines of text
rawk is not meant to adopt all features that gawk can do, the focus here will be on the string seperation
however if it proofs necessary new features will be added

## Compile
*tested with compiler version 1.49.0*
`cargo build --release --locked`

this programm is available in the Aur as rawk and rawk-git

## how to use the program
    Syntax to call the program:
    rawk [-s <seperator_char>] [-l <n>] [-f <n>] <filename>
    the program is also able to read from stdin
    -s the char following this flag will be seen as a seperator, picked in combination with -f
    -l the line number
    -f the sperator seperates the line this number chooses the fragment

multiple seperators will be considered as one: `some//////string` will result in `["some", "string"]`

## contributing
I highly value error handling, if you happen to find any error i haven't handled open an issue.
improvement suggestions/pull request are always welcome
